# scaledrone-android

Android implementation of Scaledrone (instant messaging service)

# How to use
- You first need to register an account on scaledrone.com
- Then create a channel in your dashboard
- Replace the ID in main activity's java class
- Compile & run