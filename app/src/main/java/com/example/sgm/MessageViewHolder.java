package com.example.sgm;

import android.view.View;
import android.widget.TextView;

class MessageViewHolder
{
    public View avatar;
    public TextView name;
    public TextView messageBody;
}