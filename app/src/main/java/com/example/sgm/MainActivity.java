package com.example.sgm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scaledrone.lib.Listener;
import com.scaledrone.lib.Room;
import com.scaledrone.lib.RoomListener;
import com.scaledrone.lib.Scaledrone;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements RoomListener
{
    private String channelID = "CHANNEL ID HERE";
    private String roomName = "observable-room";
    private EditText editText;
    private Scaledrone scaledrone;
    private MessageAdapter messageadap;
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // This is where we write the mesage
        editText = (EditText) findViewById(R.id.editText);
        messageadap = new MessageAdapter(this);

        lv = (ListView)findViewById(R.id.messages_view);
        lv.setAdapter(messageadap);

        MemberData data = new MemberData(getRandomName(), getRandomColor());
        scaledrone = new Scaledrone(channelID, data);

        scaledrone.connect(new Listener()
        {
            @Override
            public void onOpen()
            {
                System.out.println("Scaledrone connection open");
                // Since the MainActivity itself already implement
                // RoomListener we can pass it as a target

                scaledrone.subscribe(roomName, MainActivity.this);
            }

            @Override
            public void onOpenFailure(Exception ex)
            {
                System.err.println(ex);
            }

            @Override
            public void onFailure(Exception ex)
            {
                System.err.println(ex);
            }

            @Override
            public void onClosed(String reason)
            {
                System.err.println(reason);
            }
        });
    }

    public void sendMessage(View view)
    {
        String message = editText.getText().toString();
        if (message.length() > 0)
        {
            scaledrone.publish("observable-room", message);
            editText.getText().clear();
        }
    }

    // Successfully connected to Scaledrone room
    @Override
    public void onOpen(Room room)
    {
        System.out.println("Conneted to room");
    }

    // Connecting to Scaledrone room failed
    @Override
    public void onOpenFailure(Room room, Exception ex)
    {
        System.err.println(ex);
    }

    // Received a message from Scaledrone room
    @Override
    public void onMessage(Room room, com.scaledrone.lib.Message receivedMessage)
    {
        //Message
        // {
        // ID='6sF0j3pn8h',
        // data="ccg",
        // timestamp=1607356674,
        // clientID='8A8qMz4u3w',
        // member = Member
        // {
        //      id='8A8qMz4u3w',
        //      authData=null,
        //      clientData =
        //          {
        //             "color":"#7d5fe2",
        //             "name":"broken_butterfly"
        //          }
        //      }
        // }

        System.out.println(receivedMessage.toString());


        // To transform the raw JsonNode into a POJO we can use an ObjectMapper
        final ObjectMapper mapper = new ObjectMapper();

        try
        {


            // member.clientData is a MemberData object, let's parse it as such
            final MemberData data = mapper.treeToValue(receivedMessage.getMember().getClientData(), MemberData.class);

            // if the clientID of the message sender is the same as our's it was sent by us
            boolean belongsToCurrentUser = receivedMessage.getMember().getId().equals(scaledrone.getClientID());

            // since the message body is a simple string in our case,
            // we can use json.asText() to parse it as such
            // if it was instead an object we could use a similar pattern to data parsing

            final Message message = new Message(receivedMessage.getData().asText(), data, belongsToCurrentUser);

            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    System.out.println("from UI thread!");

                    //messageAdapter.add(message);
                    messageadap.add(message);

                    // scroll the ListView to the last added element
                    //messagesView.setSelection(messagesView.getCount() - 1);
                    lv.setSelection(lv.getCount() - 1);
                }
            });
        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
        }
    }

    private String getRandomName()
    {
        String[] adjs = {"autumn", "hidden", "bitter", "misty", "silent", "empty", "dry", "dark",
                "summer", "icy", "delicate", "quiet", "white", "cool", "spring", "winter",
                "patient", "twilight", "dawn", "crimson", "wispy", "weathered", "blue",
                "billowing", "broken", "cold", "damp", "falling", "frosty", "green",
                "long", "late", "lingering", "bold", "little", "morning", "muddy", "old",
                "red", "rough", "still", "small", "sparkling", "throbbing", "shy",
                "wandering", "withered", "wild", "black", "young", "holy", "solitary",
                "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine",
                "polished", "ancient", "purple", "lively", "nameless"};
        String[] nouns ={"waterfall", "river", "breeze", "moon", "rain", "wind", "sea", "morning",
                "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn", "glitter",
                "forest", "hill", "cloud", "meadow", "sun", "glade", "bird", "brook",
                "butterfly", "bush", "dew", "dust", "field", "fire", "flower", "firefly",
                "feather", "grass", "haze", "mountain", "night", "pond", "darkness",
                "snowflake", "silence", "sound", "sky", "shape", "surf", "thunder",
                "violet", "water", "wildflower", "wave", "water", "resonance", "sun",
                "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper",
                "frog", "smoke", "star"};

        return adjs[(int)Math.floor(Math.random() * adjs.length)] + "_" + nouns[(int)Math.floor(Math.random() * nouns.length)];
    }
    private String getRandomColor()
    {
        Random r = new Random();
        StringBuffer sb = new StringBuffer("#");
        while(sb.length() < 7)
        {
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, 7);
    }
}